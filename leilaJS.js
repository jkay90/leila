/**
 * Leila is a Abstract Testsuite for GA Experiments
 *
 * This framework helps you to configure variations and control in easy ways.
 * This currently integrated with Webtrekk.
 *
 * @author Jeyanth Kumar (jeyanth.k@lazada.com)
 * @version 0.3b
 */

/**
 * leila is a dummy stupid name that popped into my head. :P
 * @param  {string} experiment_id this is the google experiment ID
 * @param  {string} experiment_name Optional name for the experiment
 * for WT/cookies
 *
 * @this {Object}
 */
var leilaJS = function(experiment_id, experiment_name, wt, _gaq) {

    /**
     * This is the actuall experiment id of the experiment
     * @type {string}
     */
    this.experiment_name = experiment_name === undefined ?
        experiment_id : experiment_name;

    /**
     * This just have the choosen variation in place in just in case the
     * cxApi changes on multiple experiments.
     * @type {Number}
     */
    this.chosenVariation = 0;

    var ga_script = '//www.google-analytics.com/cx/api.js?experiment=' +
        experiment_id;
    /**
     * this is the test variation functions. By default its control
     * (original content) which shouldn't be altered. so the
     * function remains an empty function.
     * @type {Array}
     */
    var variations = [
        function() { }
    ];

    /**
     * this is the test variation names. It has the orignal in the slot 0
     * and other slots will have the respective variation names.
     * @type {Array}
     */
    var variations_name = [
        'Original'
    ];

    /**
     * Settings for the AB test lib
     * @type {Object}
     */
    var params = {};
    params.parseUri = {
        options: {
            strictMode: false,

            key: ['source', 'protocol', 'authority', 'userInfo', 'user',
                'password', 'host', 'port', 'relative', 'path', 'directory',
                'file', 'query', 'anchor'
            ],

            q: {
                name: 'queryKey',
                parser: /(?:^|&)([^&=]*)=?([^&]*)/g
            },

            parser: {
                strict: /^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,
                loose: /^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/)?((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/
            }
        }
    };

    /**
     * default RegEx/Url for pages where the test should Run
     * @type {Array}
     */
    var onPages = ['.*'];
    /**
     * Default RegEx/Url for pages where test should not run
     * @type {Boolean|Array|null}
     */
    var offPages = false;

    var that = this;

    /**
     * Global Utilities
     * @type {Object}
     */
    this.Utils = {};

    /**
     * Cookie settings
     * @type {Object}
     */
    var cookieParams = {};

    cookieParams.name = 'ga_exp_leila';
    cookieParams.expDelimiter = '|||';
    cookieParams.varDelimiter = '||';

    /**
     * optional method to set value for pages where test should be running
     * @param {Array} arr Array of URL's RegEx
     */
    this.setOnPages = function(arr) {
        if (typeof(arr) == 'object')
            onPages = arr;
    };

    /**
     * optional method to set value for pages where test should not be running
     * @param {Array} arr Array of URL's RegEx
     */
    this.setOffPages = function(arr) {
        if (typeof(arr) == 'object')
            offPages = arr;
    };

    /**
     * This function will send data to webtrekk on diff slots for multivariate
     * testing
     *
     * @return {boolean} False if webtrekk is not defined
     */
    var setWtSession = function() {
        if (wt === undefined)
            return false;

        var data = {
            linkId: 'TestSuite',
            customSessionParameter: {}
        };

        var slot = 9,
            slotEnd = 15;

        var cookie = readCookie(cookieParams.name);
        var experiments = cookie.split(cookieParams.expDelimiter);
        //this is something testsuite was using previoulsy bitch
        for (var i = 0; i < experiments.length; i++) {
            var exp = experiments[i].split(cookieParams.varDelimiter);
            var label = exp[0] + ' - ' + variations_name[exp[1]];

            if (slot <= slotEnd) {
                data['customSessionParameter'][slot] = label;
            } else {
                break;
            }

            ++slot;
        }
        wt.sendinfo(data);
        return true;
    };

    var setGoogleSession = function() {
        if (_gaq === undefined)
            return;

        _gaq.push(['_trackEvent',
            'GATest',
            that.experiment_name,
            variations_name[that.chosenVariation],
            undefined,
            true
        ]);

    };

    /**
     * This function checks the session cookie for the ga experiment. If
     * no session cookies is found it will create one.
     *
     * If the test is not found Then the cookie will be updated with the current
     * test.
     *
     * Whenever a session is created/updated false will be returned.
     *
     * @return {boolean} True if there is no change in session. False if new
     * session is created or current seession is updated with new test.
     */
    var checkSession = function() {
        var cookie = readCookie(cookieParams.name);

        var variation = cxApi.chooseVariation();
        var experiment_name = that.experiment_name;

        if (cookie == null) {
            //create a damn cookie.
            var value = experiment_name + cookieParams.varDelimiter + variation;
            //session level cookie
            document.cookie = cookieParams.name + '=' + value + '; path=/';
            return false;
        } else {
            var session_present = false;
            cookie.split(cookieParams.expDelimiter).forEach(function(c) {
                if (c.split(cookieParams.varDelimiter)[0] == experiment_name)
                    session_present = true;
            });

            if (session_present == false) {
                //no experment is present here bitch.. create one.
                var value = cookie + cookieParams.expDelimiter +
                    experiment_name + cookieParams.varDelimiter + variation;
                document.cookie = cookieParams.name + '=' + value + '; path=/';
                return false;
            }
        }
        return true;
    };

    /**
     * Simple copy/paste code to read a cookie.. :P
     * @param  {string} name Name of the cookie
     * @return {?string}      Content of the cookie or null if no content.
     */
    var readCookie = function(name) {
        var nameEQ = name + '=';
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) === 0)
                return c.substring(nameEQ.length, c.length);
        }
        return null;
    };

    this.Utils.updateQueryString = function(queryKey, queryValue, url) {
        if (!url) {
            url = window.location.href;
        }

        var re = new RegExp('([?|&])' + queryKey + '=.*?(&|#|$)(.*)', 'gi');

        if (re.test(url)) {
            if (typeof queryValue !== 'undefined' && queryValue !== null) {
                return url.replace(re, '$1' + queryKey + '=' +
                    queryValue + '$2$3');
            } else {
                return url.replace(re, '$1$3').replace(/(&|\?)$/, '');
            }
        } else {
            if (typeof queryValue !== 'undefined' && queryValue !== null) {
                var separator = url.indexOf('?') !== -1 ? '&' : '?',
                    hash = url.split('#');
                url = hash[0] + separator + queryKey + '=' + queryValue;
                if (hash[1])
                    url += '#' + hash[1];
                return url;
            } else {
                return url;
            }
        }
    };

    this.Utils.addParamToLinks =
        function(paramKey, paramValue, hrefContains, hrefValue) {
        $(function() {
            if (hrefContains) {
                $('a[href*="' + hrefValue + '"]').each(function() {
                    $(this).attr('href', that.Utils.updateQueryString(paramKey,
                        paramValue, $(this).attr('href')));
                });
            } else {
                $('a:not([href*="' + hrefValue + '"])').each(function() {
                    $(this).attr('href', that.Utils.updateQueryString(paramKey,
                        paramValue, $(this).attr('href')));
                });
            }
        });
    };

    this.Utils.checkPageEligibility = function() {
        var curUri = that.Utils.parseUri(window.location.href);
        var path = onPages.join('|');
        path = RegExp(path, 'g');
        var pathMatch = (curUri.path).match(path);
        if (pathMatch !== null) {
            if (offPages) {
                path = offPages.join('|');
                path = RegExp(path, 'g');
                pathMatch = (curUri.path).match(path);
                if (pathMatch !== null)
                    return false;
                else
                    return true;
            } else
                return true;
        } else
            return false;
    };

    this.Utils.parseUri = function(str) {
        var o = params.parseUri.options,
            m = o.parser[o.strictMode ? 'strict' : 'loose'].exec(str),
            uri = {},
            i = 14;
        while (i--) uri[o.key[i]] = m[i] || '';
        uri[o.q.name] = {};
        uri[o.key[12]].replace(o.q.parser, function($0, $1, $2) {
            if ($1)
                uri[o.q.name][$1] = $2;
        });
        return uri;
    };

    /**
     * Set variation function for the Google Experiment
     * @param {Function} fn Fuction that does the necessary
     * modification for the page
     * @param {String} name Name of the variation. Used for the GA/WT session
     *
     * If variation name is not given. Default variation prefix will be used.
     *
     * @return {Boolean} True if all went well else false.
     */
    this.setVariation = function(fn, name) {
        if (typeof(fn) == 'function')
            variations.push(fn);
        else
            return false;

        if (typeof(name) == 'string')
            variations_name.push(name);
        else {
            var index = variations_name.length;
            variations_name.push('Variation ' + index);
        }
        return true;
    };

    /**
     * This is the main funciton that executes the Google Experiment.
     */
    this.exec = function() {
        if ($ === undefined)
            return;

        if (that.Utils.checkPageEligibility() === false)
            return;

        $.getScript(ga_script).done(function() {
            if (cxApi === undefined) {
                console.log('cxApi is undefined');
                return;
            }
            that.chosenVariation = cxApi.chooseVariation();
            //console.log('We have a winner : ' + chosenVariation);
            variations[that.chosenVariation]();
            //check if cookie session is created. if not one is created and
            //returned false
            //when a new session is created we need to set WT parameters.
            if (checkSession() === false) {
                setWtSession();
                setGoogleSession();
            }
        }).fail(function() {
            console.log('leilaError: Unable to get GA Experiment');
        });
    };
};

