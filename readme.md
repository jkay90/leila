Leilaa JS 
=========

AB Test made easy with Google analytics experiments. Leila is a Abstract Testsuite 
for GA experiments.

###Why Leila
 * Leila will help you to run google experiments without reloading your page. 
 * Leila is integrated with google analytics events for more customized
 reports which you can do in analytics dashboard. It has webtrekk integration as
 well
 * You can put the code on all the pages and allow the test to be run only on 
 specific page. 

###Usage

Initialize the ab test by its experiment id and its name. 
```javascript
ab_test = new leliaJS('experiment_id', 'experiment_name')
```

Now you can add the variation by calling setVariation method from the 
ab_test object.

```javascript
ab_test.setVariation(function(){  
    doVariation();  
    }, "Variation Label");
```


You can add multiple variation as you configured in the google experiments in the
analytics dashboard.

Execute the experiment by calling exec

```javascript
ab_test.exec()
```

**Note**

 * The default control is an empty function and does nothing and user doesn't have 
 to configure it.
 * The traffic split % is confirgured in the dashboard. You have to choose the ones
which are avaialbe in google analytics. 

###Advanced Usage

**These are optionals. Don't really have to use them to run the test**

You can decide which page should run the experiments and which page should not,
These operations are done inside using regex. And the urls to be excluded or 
included to be specified on the methods which are defined as follows. 

To run the tests only on certain pages use `setOnPages` method to define. And it
accpets array of regex of URLs. By default its `.*`

```javascript
ab_test.setOnPages(['/catalog/*', '/all-product/*'])
```

To exclude certain pages from running the tests use `setOffPages` method. 
Use array of urls as an argument. 

```javascript
ab_test.setOffPages(['/faq/', '/contact/'])
```


###Author
[Jeyanth Kumar K](https://www.facebook.com/jeyanthkumar.k)([jeyanth.kmr@gmail.com](mailto:jeyanth.kmr@gmail.com)})
